package com.palominolabs.jackson.datatype.trove;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeBindings;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.type.TypeModifier;
import gnu.trove.map.TByteObjectMap;
import gnu.trove.map.TCharObjectMap;
import gnu.trove.map.TDoubleObjectMap;
import gnu.trove.map.TFloatObjectMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.TObjectByteMap;
import gnu.trove.map.TObjectCharMap;
import gnu.trove.map.TObjectDoubleMap;
import gnu.trove.map.TObjectFloatMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.TObjectShortMap;
import gnu.trove.map.TShortObjectMap;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Lets TObject*Map be map-like.
 */

class TroveTypeModifier extends TypeModifier {
    private final static Map<Class<?>, Class<?>> objKeyMapsToValTypes = new HashMap<Class<?>, Class<?>>();
    static {
        objKeyMapsToValTypes.put(TObjectIntMap.class, int.class);
        objKeyMapsToValTypes.put(TObjectShortMap.class, short.class);
        objKeyMapsToValTypes.put(TObjectLongMap.class, long.class);
        objKeyMapsToValTypes.put(TObjectFloatMap.class, float.class);
        objKeyMapsToValTypes.put(TObjectDoubleMap.class, double.class);
        objKeyMapsToValTypes.put(TObjectByteMap.class, byte.class);
        objKeyMapsToValTypes.put(TObjectCharMap.class, char.class);
    }
    private final static Map<Class<?>, Class<?>> objValMapsToKeyTypes = new HashMap<Class<?>, Class<?>>();
    static {
        objValMapsToKeyTypes.put(TIntObjectMap.class, int.class);
        objValMapsToKeyTypes.put(TShortObjectMap.class, short.class);
        objValMapsToKeyTypes.put(TLongObjectMap.class, long.class);
        objValMapsToKeyTypes.put(TFloatObjectMap.class, float.class);
        objValMapsToKeyTypes.put(TDoubleObjectMap.class, double.class);
        objValMapsToKeyTypes.put(TByteObjectMap.class, byte.class);
        objValMapsToKeyTypes.put(TCharObjectMap.class, char.class);
    }

    TroveTypeModifier() {
    }
    
    @Override
    public JavaType modifyType(JavaType type, Type jdkType, TypeBindings context, TypeFactory typeFactory) {

        Class<?> rawClass = type.getRawClass();
        for (Map.Entry<Class<?>, Class<?>> classTypeEntry : objKeyMapsToValTypes.entrySet()) {
            Class<?> mapLikeClass = classTypeEntry.getKey();
            if (mapLikeClass.isAssignableFrom(rawClass)) {
                JavaType keyType  = _findTypeParam(typeFactory, type, mapLikeClass, 0);
                return typeFactory.constructMapLikeType
                        (mapLikeClass, keyType, typeFactory.constructType(classTypeEntry.getValue()));
            }
        }
        for (Map.Entry<Class<?>, Class<?>> classTypeEntry : objValMapsToKeyTypes.entrySet()) {
            Class<?> mapLikeClass = classTypeEntry.getKey();
            if (mapLikeClass.isAssignableFrom(rawClass)) {
                JavaType valueType  = _findTypeParam(typeFactory, type, mapLikeClass, 0);
                return typeFactory.constructMapLikeType
                        (mapLikeClass, typeFactory.constructType(classTypeEntry.getValue()), valueType);
            }
        }
        return type;

    }

    protected JavaType _findTypeParam(TypeFactory typeFactory, JavaType baseType, Class<?> targetType,
            int index)
    {
        JavaType[] typeParameters = typeFactory.findTypeParameters(baseType, targetType);
        if (typeParameters == null || typeParameters.length <= index) {
            return TypeFactory.unknownType();
        }
        return typeParameters[index];
    }
}
