package com.palominolabs.jackson.datatype.trove;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.palominolabs.jackson.datatype.trove.deser.TroveDeserializers;
import com.palominolabs.jackson.datatype.trove.ser.TroveSerializers;

public final class TroveModule extends SimpleModule {
    private static final long serialVersionUID = 1L;

    public TroveModule() {
        super("TroveModule", ModuleVersion.INSTANCE.version());
    }

    @Override
    public void setupModule(SetupContext context) {
        super.setupModule(context);

        context.addTypeModifier(new TroveTypeModifier());

        context.addSerializers(new TroveSerializers());
        context.addDeserializers(new TroveDeserializers());
    }
}
