package com.palominolabs.jackson.datatype.trove.deser;

import com.fasterxml.jackson.databind.*;

import com.fasterxml.jackson.datatype.joda.JodaModule;

import com.palominolabs.jackson.datatype.trove.TroveModule;

import org.junit.Before;

public abstract class BaseTroveDeserializerTest {
    ObjectReader reader;

    @Before
    public void setUp() {
        reader = new ObjectMapper().registerModule(new TroveModule()).registerModule(new JodaModule()).reader();
    }
}
